#pragma once

#define MAX_OBJ_COUNT 1000

#define HERO_ID 0
#define MONSTER_ID 1

#define GRAVITY 9.8f

#define TYPE_NORMAL 0

#define TYPE_BULLET 1

#define TYPE_MONSTER 2


#define MAX_TEXTURES 1000