#include "stdafx.h"
#include "Object.h"
#include "Globals.h"
#include <math.h>
#include <iostream>

Object::Object()
{
	InitPhysics();
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
}


Object::~Object()
{

}
void Object::SetType(int type)
{
	m_type = type;
}
void Object::GetType(int* type)
{
	*type = m_type;
}

void Object::SetPosX(float x)
{
	m_posX = x;
}

void Object::SetPosY(float y)
{
	m_posY = y;
}

void Object::SetPosZ(float z)
{
	m_posZ = z;
}

float Object::GetPosX()
{
	return m_posX;
}

float Object::GetPosY()
{
	return m_posY;
}

float Object::GetPosZ()
{
	return m_posZ;
}

bool Object::CanShootBullet()
{
	if (m_remainingBulletCoolTime < FLT_EPSILON)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Object::ResetBulletCoolTime()
{
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
}



void Object::SetParentObj(Object* object)
{
	m_parent = object;
}

Object* Object::GetParentObj() const
{
	return m_parent ;
}


void Object::SetFricCoef(float coef)
{
	m_fricCoef = coef;
}

void Object::GetFricCoef(float* coef)
{
	*coef = m_fricCoef;
}

void Object::AddForce(float x, float y, float z, float eTime)
{
	float accX, accY, accZ;			//가속도 구하기
	accX = accY = accZ = 0.f;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * eTime;
	m_velY = m_velY + accY * eTime;
	m_velZ = m_velZ + accZ * 0.1f; //2d적용 - 점프구현하면 원래대로 돌려놓기. 

	std::cout << "vel X" << m_velX << std::endl;
	std::cout << "vel Y" << m_velY << std::endl;

	accX = 0.f;
	accY = 0.f;
	accZ = 0.f;
}

void Object::Update(float elapsedInsec)// 틀렸을 확률
{
	m_remainingBulletCoolTime -= elapsedInsec; // 쿨타임 적용



	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY + m_velZ * m_velZ);
	//예외처리
	if (velSize > FLT_EPSILON)
	{
		float vX = m_velX / velSize;
		float vY = m_velY / velSize;
		float vZ = m_velZ / velSize;

		float nForce = m_mass * GRAVITY;
		float fricForce = m_fricCoef * nForce;
		
		float fricX = -vX * fricForce;
		float fricY = -vY * fricForce;
		//float fricZ = vZ * fricForce;

		float accX = fricX / m_mass;
		float accY = fricY / m_mass;

		float newX = m_velX + accX * elapsedInsec;
		float newY = m_velY + accY * elapsedInsec;

		if (newX * m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		//else if(abs(m_velX) > 0.5f)
		//{
		//	m_velX = 0.5f;
		//}
		else
		{
			m_velX = newX;
		}

		if (newY * m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		//else if (abs(m_velY)> 0.5f)
		//{
		//	m_velY = 0.5f;
		//}
		else
		{
			m_velY = newY;
		}
	}
	else
	{
		m_velX = 0;
		m_velY = 0;
		m_velZ = 0;
	}

	m_posX = m_posX + m_velX * elapsedInsec;
	m_posY = m_posY + m_velY * elapsedInsec;
	m_posZ = m_posZ + m_velZ * elapsedInsec;
	int type;
	this->GetType(&type);
	std::cout << type << "object: (" << m_posX << ", " << m_posY << ", " << m_posZ << ")" << std::endl;

}

void Object::InitPhysics()
{
	m_posX = 0.f, m_posY = 0.f, m_posZ = 0.f;	 // position x,y,z 좌표 ( location )
	m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f;	 // velocity ( 속도 )
	m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f;	 // acceleration
	m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f;	 // volume
	m_r = 0.f, m_g = 0.f, m_b = 0.f, m_a = 0.f;		 // color -1?
	m_mass = 0.f;			  		 // mass -1?
	m_fricCoef = 0.f;				// 마찰계수 0
}

void Object::SetVol(float x, float y, float z)
{
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}
void Object::GetVol(float* x, float* y, float* z)
{
	*x = m_volX;
	*y = m_volY;
	*z = m_volZ;
}
void Object::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}
void Object::GetPos(float* x, float* y, float* z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}
void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Object::SetAcc(float x, float y, float z)
{
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}

void Object::GetAcc(float* x, float* y, float* z)
{
	*x = m_accX;
	*y = m_accY;
	*z = m_accZ;
}

void Object::SetVel(float x, float y, float z)
{
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}

void Object::GetVel(float* x, float* y, float* z)
{
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}

void Object::GetMass(float* mass)
{
	*mass = m_mass;
}
