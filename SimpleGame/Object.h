#pragma once
class Object
{
public:

	Object();
	~Object();

	void Update(float elapsedInsec);
	void AddForce(float x, float y, float z, float eTime);
	void InitPhysics();

	void SetVol(float x, float y, float z);
	void GetVol(float* x, float* y, float* z);
	void SetPos(float x, float y, float z);
	void GetPos(float* x, float* y, float* z);
	void SetVel(float x, float y, float z);
	void GetVel(float* x, float* y, float* z);
	void SetAcc(float x, float y, float z);
	void GetAcc(float* x, float* y, float* z);
	void SetColor(float r, float g, float b, float a);
	void GetColor(float* r, float* g, float* b, float* a);
	void SetMass(float mass);
	void GetMass(float* mass);
	void SetFricCoef(float coef);
	void GetFricCoef(float* coef);
	void SetType(int type);
	void GetType(int* type);

	void SetPosX(float x);
	void SetPosY(float y);
	void SetPosZ(float z);

	//void GetPosX(float* x);
	//void GetPosY(float* y);
	//void GetPosZ(float* z);

	float GetPosX();
	float GetPosY();
	float GetPosZ();

	bool CanShootBullet();
	void ResetBulletCoolTime();
	void SetParentObj(Object* object);
	Object* GetParentObj() const;



private:
	float m_posX{}, m_posY{}, m_posZ{};	 // position x,y,z 좌표
	float m_velX{}, m_velY{}, m_velZ{};	 // velocity 속도
	float m_accX{}, m_accY{}, m_accZ{};	 // acceleration 가속도
	float m_volX{}, m_volY{}, m_volZ{};	 // volume 부피
	float m_r{}, m_g{}, m_b{}, m_a{};		 // color 색상
	float m_mass{};			  		 // mass 질량
	float m_fricCoef{};				 // friction coefficient 마찰계수
	int m_type{};						 // object type

	float m_remainingBulletCoolTime = 0.f;
	float m_defaultBulletCoolTime = 0.2f;

	int m_texID = -1;

	Object* m_parent;
};

