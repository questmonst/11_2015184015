#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include "Object.h"
#include "Renderer.h"

#include <float.h>
#include "Physics.h"

// float.h에는 FLT_EPSILON define 되어있음 (0.000001f 대신 쓰면됨)

ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(1000, 1000);

	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}
	m_Physics = new Physics();

	// Initialize Objects
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		m_Obj[i] = NULL;
	}

	// Create Hero
	m_Obj[HERO_ID] = new Object();
	m_Obj[HERO_ID]->SetPos(0, 0, 0);
	m_Obj[HERO_ID]->SetVol(0.25, 0.25, 0.25);
	m_Obj[HERO_ID]->SetColor(1, 1, 1, 1);
	m_Obj[HERO_ID]->SetVel(0, 0, 0);
	m_Obj[HERO_ID]->SetMass(1.2f);
	m_Obj[HERO_ID]->SetFricCoef(0.7f);
	m_Obj[HERO_ID]->SetType(TYPE_NORMAL);

	//create test monster
	int temp = AddObject(
		0.01f, 0.01f, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		0, 0, 0,
		1.f,
		0.7,
		TYPE_MONSTER);

}

ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL)
	{
		delete m_Renderer;
		m_Renderer = NULL;
	}
}
void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = true;
	}
	if (key == 's' || key == 'S')
	{
		m_KeyS = true;
	}
	if (key == 'a' || key == 'A')
	{
		m_KeyA = true;
	}
	if (key == 'd' || key == 'D')
	{
		m_KeyD = true;
	}
}
void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = false;
	}
	if (key == 's' || key == 'S')
	{
		m_KeyS = false;
	}
	if (key == 'a' || key == 'A')
	{
		m_KeyA = false;
	}
	if (key == 'd' || key == 'D')
	{
		m_KeyD = false;
	}
}
void ScnMgr::SpecialKeyDownInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = true;
	}
}
void ScnMgr::SpecialKeyUpInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = false;
	}
}

void ScnMgr::Update(float elapsedInSec)
{
	//캐릭터 컨트롤 감지
	//std::cout << "W:" << m_KeyW << ", A:" << m_KeyA << ", S:" << m_KeyS << ", D:" << m_KeyD << std::endl;
	//std::cout << "d:" << m_KeyDown << ", u:" << m_KeyUp << ", l:" << m_KeyLeft << ", R:" << m_KeyRight << std::endl;
	//

	//
	float fx, fy, fz;
	fx = fy = fz = 0.f;
	m_Obj[HERO_ID]->GetPos(&fx,&fy,&fz);
	m_Obj[HERO_ID]->SetAcc(0,0,0);
	float fAmount = 10.0f;
	
		if (m_KeyW)
		{
			fy += 1.f;
		}
		if (m_KeyS)
		{
			fy -= 1.f;
		}
		if (m_KeyA)
		{
			fx -= 1.f;
		}
		if (m_KeyD)
		{
			fx += 1.f;
		}

	
	const float fSize = std::sqrt(fx * fx + fy * fy + fz * fz);
	if (fSize > FLT_EPSILON)
	{
		fx /= fSize;
		fy /= fSize;
		fz /= fSize; // z 추후 변경

		fx *= fAmount;
		fy *= fAmount;
		fz *= fAmount;

		m_Obj[HERO_ID]->AddForce(fx, fy, fz, elapsedInSec);
	}

	//Fire bullets
	if (m_Obj[HERO_ID]->CanShootBullet())
	{
		//std::cout << "key" << std::endl;
		float bulletVel = 1.0f;		//5mps
		float vBulletX, vBulletY, vBulletZ; //bullet vel
		vBulletX = vBulletY = vBulletZ = 0.f;
		if (m_KeyUp)
		{
			vBulletY += 1.f;
		}
		if (m_KeyDown)
		{
			vBulletY -= 1.f;
		}
		if (m_KeyLeft)
		{
			vBulletX -= 1.f;
		}
		if (m_KeyRight)
		{
			vBulletX += 1.f;
		}

		float vBulletSize =
			sqrtf(vBulletX * vBulletX + vBulletY * vBulletY);

		if (vBulletSize > FLT_EPSILON)
		{
			//std::cout << "key" << std::endl;
			vBulletX /= vBulletSize;
			vBulletY /= vBulletSize;
			vBulletZ /= vBulletSize;

			vBulletX *= bulletVel;
			vBulletY *= bulletVel;
			vBulletZ *= bulletVel;

			float hvX, hvY, hvZ;
			m_Obj[HERO_ID]->GetVel(&hvX, &hvY, &hvZ);
			hvX = hvX + vBulletX;
			hvY = hvY + vBulletY;
			hvZ = hvZ + vBulletZ;

			float hX, hY, hZ;
			m_Obj[HERO_ID]->GetPos(&hX, &hY, &hZ);



			std::cout << "vBulletX : " << vBulletX << std::endl;
			std::cout << "vBulletY : " << vBulletY << std::endl;
			std::cout << "vBulletZ : " << vBulletZ << std::endl;

			int ID = AddObject(
				hX, hY, hZ,
				0.05f, 0.05f, 0.05f,
				1.0f, 0.0f, 0.0f, 1,
				vBulletX, vBulletY, vBulletZ,
				1.f,
				0.7f,
				TYPE_BULLET
			);
			m_Obj[ID]->AddForce(vBulletX, vBulletY, vBulletZ, 0.1f);
			m_Obj[ID]->SetParentObj(m_Obj[HERO_ID]);
			m_Obj[HERO_ID]->ResetBulletCoolTime();
		}

	}





	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		for (int j = i + 1; j < MAX_OBJ_COUNT; ++j)
		{
			if (m_Obj[i] != NULL && m_Obj[j] != NULL)
			{
				if (m_Physics->IsOverlap(m_Obj[i], m_Obj[j]))
				{
					if (!(m_Obj[i]->GetParentObj() == m_Obj[j] || m_Obj[j]->GetParentObj() == m_Obj[i]))
					{
						m_Physics->ProcessCollision(m_Obj[i], m_Obj[j]);
						std::cout << "collision " << i << ", " << j << std::endl;

					}

				}
			}
		}
	}
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
		{
			m_Obj[i]->Update(elapsedInSec);
		}
	}
	fx = fy = fz = 0.f;

}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Draw all m_Obj
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
		{
			float x, y, z = 0;
			float sx, sy, sz = 0;
			float r, g, b, a = 0;
			m_Obj[i]->GetPos(&x, &y, &z);
			m_Obj[i]->GetVol(&sx, &sy, &sz);
			m_Obj[i]->GetColor(&r, &g, &b, &a);

			// 1meter == 100cm == 100 pixels
			x = x * 10.f;
			y = y * 10.f;
			z = z * 10.f;

			{
				if (i != HERO_ID)
				{
					std::cout << "RenderScene: vel: " << x << ", " << y << ", " << z << ": " << i << std::endl;
				}
			}

			sx = sx * 100.f;
			sy = sy * 100.f;
			sz = sz * 100.f;

			{
				int type;
				m_Obj[i]->GetType(&type);
			}


			m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);
		}
	}
}
void ScnMgr::DoGarbageCollection()
{
	// delete bullets those zero vel
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] != NULL)
		{
			int type;
			m_Obj[i]->GetType(&type);
			if (type == TYPE_BULLET)
			{
				float vx, vy, vz;
				m_Obj[i]->GetVel(&vx, &vy, &vz);
				float vSize = sqrtf(vx * vx + vy * vy + vz * vz);
				if (vSize < FLT_EPSILON)
				{
					DeleteObject(i);
					std::cout << "deleted : " << i << std::endl;
				}
			}
		}
	}
}

int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float mass,
	float fricCoef,
	int type)
{
	int idx = -1;
	for (int i = 0; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] == NULL)
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
	{
		std::cout << "No more empty obj slot. \n";
		return -1;
	}

	m_Obj[idx] = new Object();
	m_Obj[idx]->SetPos(x, y, z);
	m_Obj[idx]->SetVol(sx, sy, sz);
	m_Obj[idx]->SetColor(r, g, b, a);
	m_Obj[idx]->SetVel(vx, vy, vz);
	m_Obj[idx]->SetMass(mass);
	m_Obj[idx]->SetFricCoef(fricCoef);
	m_Obj[idx]->SetType(type);

	return idx;
}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0)
	{
		std::cout << "input idx is negative : " << idx << std::endl;
		return;
	}

	if (idx >= MAX_OBJ_COUNT)
	{
		std::cout << "input idx exceeds MAX_OBJ_COUNT : " << idx << std::endl;
		return;
	}

	if (m_Obj[idx] == NULL)
	{
		std::cout << "m_Obj[" << idx << "] is NULL." << std::endl;
		return;
	}

	delete m_Obj[idx];
	m_Obj[idx] = NULL;
}