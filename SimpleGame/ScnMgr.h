#pragma once
#include "Object.h"
#include "Renderer.h"
#include "Globals.h"
#include "Physics.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();


	void Update(float elapsedInSec);
	void RenderScene();
	void DoGarbageCollection();


	int AddObject(float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass,
		float fricCoef, int type);
	void DeleteObject(int idx); // idx = index 해당되는 array 지울수있도록



	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(unsigned char key, int x, int y);
	void SpecialKeyUpInput(unsigned char key, int x, int y);

private:

	Renderer* m_Renderer = NULL;
	int m_RectPosX = 0;
	Object* m_TestObject = NULL;
	int m_TestIdx = -1;
	int m_TestIdxArray[MAX_OBJ_COUNT];
	Object* m_Obj[MAX_OBJ_COUNT];

	Physics* m_Physics = NULL;

	bool m_KeyW = false;
	bool m_KeyA = false;
	bool m_KeyS = false;
	bool m_KeyD = false;

	bool m_KeyUp = false;
	bool m_KeyDown = false;
	bool m_KeyLeft = false;
	bool m_KeyRight = false;
	// bool m_KeySP = false;

};
